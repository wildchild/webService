/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "aide")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aide.findAll", query = "SELECT a FROM Aide a")
    , @NamedQuery(name = "Aide.findByIdPub", query = "SELECT a FROM Aide a WHERE a.idPub = :idPub")
    , @NamedQuery(name = "Aide.findByDatepub", query = "SELECT a FROM Aide a WHERE a.datepub = :datepub")
    , @NamedQuery(name = "Aide.findByDescription", query = "SELECT a FROM Aide a WHERE a.description = :description")
    , @NamedQuery(name = "Aide.findByDocument", query = "SELECT a FROM Aide a WHERE a.document = :document")
    , @NamedQuery(name = "Aide.findBySection", query = "SELECT a FROM Aide a WHERE a.section = :section")
    , @NamedQuery(name = "Aide.findByMatiere", query = "SELECT a FROM Aide a WHERE a.matiere = :matiere")})
public class Aide implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPub")
    private Integer idPub;
    @Column(name = "datepub")
    @Temporal(TemporalType.DATE)
    private Date datepub;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "document")
    private String document;
    @Size(max = 255)
    @Column(name = "section")
    private String section;
    @Size(max = 255)
    @Column(name = "matiere")
    private String matiere;
    @JoinColumn(name = "idUsr", referencedColumnName = "id")
    @ManyToOne
    private User idUsr;

    public Aide() {
    }

    public Aide(Integer idPub) {
        this.idPub = idPub;
    }

    public Integer getIdPub() {
        return idPub;
    }

    public void setIdPub(Integer idPub) {
        this.idPub = idPub;
    }

    public Date getDatepub() {
        return datepub;
    }

    public void setDatepub(Date datepub) {
        this.datepub = datepub;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public User getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(User idUsr) {
        this.idUsr = idUsr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPub != null ? idPub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aide)) {
            return false;
        }
        Aide other = (Aide) object;
        if ((this.idPub == null && other.idPub != null) || (this.idPub != null && !this.idPub.equals(other.idPub))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Aide[ idPub=" + idPub + " ]";
    }
    
}
