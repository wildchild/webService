/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "colocation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Colocation.findAll", query = "SELECT c FROM Colocation c")
    , @NamedQuery(name = "Colocation.findByIdPub", query = "SELECT c FROM Colocation c WHERE c.idPub = :idPub")
    , @NamedQuery(name = "Colocation.findByIdUsr", query = "SELECT c FROM Colocation c WHERE c.idUsr = :idUsr")
    , @NamedQuery(name = "Colocation.findByDatepub", query = "SELECT c FROM Colocation c WHERE c.datepub = :datepub")
    , @NamedQuery(name = "Colocation.findByEtat", query = "SELECT c FROM Colocation c WHERE c.etat = :etat")
    , @NamedQuery(name = "Colocation.findByLieu", query = "SELECT c FROM Colocation c WHERE c.lieu = :lieu")
    , @NamedQuery(name = "Colocation.findByLoyermensuel", query = "SELECT c FROM Colocation c WHERE c.loyermensuel = :loyermensuel")
    , @NamedQuery(name = "Colocation.findByTypelogement", query = "SELECT c FROM Colocation c WHERE c.typelogement = :typelogement")
    , @NamedQuery(name = "Colocation.findByTypechambre", query = "SELECT c FROM Colocation c WHERE c.typechambre = :typechambre")
    , @NamedQuery(name = "Colocation.findByNbchambre", query = "SELECT c FROM Colocation c WHERE c.nbchambre = :nbchambre")
    , @NamedQuery(name = "Colocation.findByCommodite", query = "SELECT c FROM Colocation c WHERE c.commodite = :commodite")
    , @NamedQuery(name = "Colocation.findByDatedisponibilite", query = "SELECT c FROM Colocation c WHERE c.datedisponibilite = :datedisponibilite")})
public class Colocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PUB")
    private Integer idPub;
    @Column(name = "ID_USR")
    private Integer idUsr;
    @Column(name = "DATEPUB")
    @Temporal(TemporalType.DATE)
    private Date datepub;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 20)
    @Column(name = "ETAT")
    private String etat;
    @Size(max = 50)
    @Column(name = "LIEU")
    private String lieu;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LOYERMENSUEL")
    private Double loyermensuel;
    @Size(max = 50)
    @Column(name = "TYPELOGEMENT")
    private String typelogement;
    @Size(max = 50)
    @Column(name = "TYPECHAMBRE")
    private String typechambre;
    @Lob
    @Size(max = 65535)
    @Column(name = "IMGCOUVERTURE")
    private String imgcouverture;
    @Column(name = "NBCHAMBRE")
    private Integer nbchambre;
    @Size(max = 250)
    @Column(name = "COMMODITE")
    private String commodite;
    @Column(name = "DATEDISPONIBILITE")
    @Temporal(TemporalType.DATE)
    private Date datedisponibilite;
    @JoinColumn(name = "ID_VILLE", referencedColumnName = "ID_VILLE")
    @ManyToOne
    private Ville idVille;

    public Colocation() {
    }

    public Colocation(Integer idPub) {
        this.idPub = idPub;
    }

    public Integer getIdPub() {
        return idPub;
    }

    public void setIdPub(Integer idPub) {
        this.idPub = idPub;
    }

    public Integer getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(Integer idUsr) {
        this.idUsr = idUsr;
    }

    public Date getDatepub() {
        return datepub;
    }

    public void setDatepub(Date datepub) {
        this.datepub = datepub;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public Double getLoyermensuel() {
        return loyermensuel;
    }

    public void setLoyermensuel(Double loyermensuel) {
        this.loyermensuel = loyermensuel;
    }

    public String getTypelogement() {
        return typelogement;
    }

    public void setTypelogement(String typelogement) {
        this.typelogement = typelogement;
    }

    public String getTypechambre() {
        return typechambre;
    }

    public void setTypechambre(String typechambre) {
        this.typechambre = typechambre;
    }

    public String getImgcouverture() {
        return imgcouverture;
    }

    public void setImgcouverture(String imgcouverture) {
        this.imgcouverture = imgcouverture;
    }

    public Integer getNbchambre() {
        return nbchambre;
    }

    public void setNbchambre(Integer nbchambre) {
        this.nbchambre = nbchambre;
    }

    public String getCommodite() {
        return commodite;
    }

    public void setCommodite(String commodite) {
        this.commodite = commodite;
    }

    public Date getDatedisponibilite() {
        return datedisponibilite;
    }

    public void setDatedisponibilite(Date datedisponibilite) {
        this.datedisponibilite = datedisponibilite;
    }

    public Ville getIdVille() {
        return idVille;
    }

    public void setIdVille(Ville idVille) {
        this.idVille = idVille;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPub != null ? idPub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Colocation)) {
            return false;
        }
        Colocation other = (Colocation) object;
        if ((this.idPub == null && other.idPub != null) || (this.idPub != null && !this.idPub.equals(other.idPub))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Colocation[ idPub=" + idPub + " ]";
    }
    
}
