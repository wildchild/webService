/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "commentaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commentaire.findAll", query = "SELECT c FROM Commentaire c")
    , @NamedQuery(name = "Commentaire.findByIdCom", query = "SELECT c FROM Commentaire c WHERE c.idCom = :idCom")
    , @NamedQuery(name = "Commentaire.findByDateCom", query = "SELECT c FROM Commentaire c WHERE c.dateCom = :dateCom")
    , @NamedQuery(name = "Commentaire.findByIdPub", query = "SELECT c FROM Commentaire c WHERE c.idPub = :idPub")
    , @NamedQuery(name = "Commentaire.findByIdUsr", query = "SELECT c FROM Commentaire c WHERE c.idUsr = :idUsr")
    , @NamedQuery(name = "Commentaire.findByUsername", query = "SELECT c FROM Commentaire c WHERE c.username = :username")})
public class Commentaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COM")
    private Integer idCom;
    @Lob
    @Size(max = 65535)
    @Column(name = "CONTENU_COM")
    private String contenuCom;
    @Column(name = "DATE_COM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PUB")
    private int idPub;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USR")
    private int idUsr;
    @Size(max = 255)
    @Column(name = "username")
    private String username;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "photoprofile")
    private String photoprofile;

    public Commentaire() {
    }

    public Commentaire(Integer idCom) {
        this.idCom = idCom;
    }

    public Commentaire(Integer idCom, int idPub, int idUsr) {
        this.idCom = idCom;
        this.idPub = idPub;
        this.idUsr = idUsr;
    }

    public Integer getIdCom() {
        return idCom;
    }

    public void setIdCom(Integer idCom) {
        this.idCom = idCom;
    }

    public String getContenuCom() {
        return contenuCom;
    }

    public void setContenuCom(String contenuCom) {
        this.contenuCom = contenuCom;
    }

    public Date getDateCom() {
        return dateCom;
    }

    public void setDateCom(Date dateCom) {
        this.dateCom = dateCom;
    }

    public int getIdPub() {
        return idPub;
    }

    public void setIdPub(int idPub) {
        this.idPub = idPub;
    }

    public int getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(int idUsr) {
        this.idUsr = idUsr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhotoprofile() {
        return photoprofile;
    }

    public void setPhotoprofile(String photoprofile) {
        this.photoprofile = photoprofile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCom != null ? idCom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commentaire)) {
            return false;
        }
        Commentaire other = (Commentaire) object;
        if ((this.idCom == null && other.idCom != null) || (this.idCom != null && !this.idCom.equals(other.idCom))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Commentaire[ idCom=" + idCom + " ]";
    }
    
}
