/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "covoiturage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Covoiturage.findAll", query = "SELECT c FROM Covoiturage c")
    , @NamedQuery(name = "Covoiturage.findByIdPub", query = "SELECT c FROM Covoiturage c WHERE c.idPub = :idPub")
    , @NamedQuery(name = "Covoiturage.findByIdUsr", query = "SELECT c FROM Covoiturage c WHERE c.idUsr = :idUsr")
    , @NamedQuery(name = "Covoiturage.findByDatepub", query = "SELECT c FROM Covoiturage c WHERE c.datepub = :datepub")
    , @NamedQuery(name = "Covoiturage.findByEtat", query = "SELECT c FROM Covoiturage c WHERE c.etat = :etat")
    , @NamedQuery(name = "Covoiturage.findByLieudepart", query = "SELECT c FROM Covoiturage c WHERE c.lieudepart = :lieudepart")
    , @NamedQuery(name = "Covoiturage.findByLieuarrive", query = "SELECT c FROM Covoiturage c WHERE c.lieuarrive = :lieuarrive")
    , @NamedQuery(name = "Covoiturage.findByDatedepart", query = "SELECT c FROM Covoiturage c WHERE c.datedepart = :datedepart")
    , @NamedQuery(name = "Covoiturage.findByPrix", query = "SELECT c FROM Covoiturage c WHERE c.prix = :prix")
    , @NamedQuery(name = "Covoiturage.findByNbplace", query = "SELECT c FROM Covoiturage c WHERE c.nbplace = :nbplace")})
public class Covoiturage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PUB")
    private Integer idPub;
    @Column(name = "ID_USR")
    private Integer idUsr;
    @Column(name = "DATEPUB")
    @Temporal(TemporalType.DATE)
    private Date datepub;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 20)
    @Column(name = "ETAT")
    private String etat;
    @Size(max = 50)
    @Column(name = "LIEUDEPART")
    private String lieudepart;
    @Size(max = 50)
    @Column(name = "LIEUARRIVE")
    private String lieuarrive;
    @Column(name = "DATEDEPART")
    @Temporal(TemporalType.DATE)
    private Date datedepart;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRIX")
    private Double prix;
    @Lob
    @Size(max = 65535)
    @Column(name = "CHECKPOINTS")
    private String checkpoints;
    @Column(name = "NBPLACE")
    private Integer nbplace;
    @ManyToMany(mappedBy = "covoiturageCollection")
    private Collection<Ville> villeCollection;
    @JoinColumn(name = "VIL_ID_VILLE", referencedColumnName = "ID_VILLE")
    @ManyToOne
    private Ville vilIdVille;
    @JoinColumn(name = "ID_VILLE_ARR", referencedColumnName = "ID_VILLE")
    @ManyToOne
    private Ville idVilleArr;

    public Covoiturage() {
    }

    public Covoiturage(Integer idPub) {
        this.idPub = idPub;
    }

    public Integer getIdPub() {
        return idPub;
    }

    public void setIdPub(Integer idPub) {
        this.idPub = idPub;
    }

    public Integer getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(Integer idUsr) {
        this.idUsr = idUsr;
    }

    public Date getDatepub() {
        return datepub;
    }

    public void setDatepub(Date datepub) {
        this.datepub = datepub;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getLieudepart() {
        return lieudepart;
    }

    public void setLieudepart(String lieudepart) {
        this.lieudepart = lieudepart;
    }

    public String getLieuarrive() {
        return lieuarrive;
    }

    public void setLieuarrive(String lieuarrive) {
        this.lieuarrive = lieuarrive;
    }

    public Date getDatedepart() {
        return datedepart;
    }

    public void setDatedepart(Date datedepart) {
        this.datedepart = datedepart;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(String checkpoints) {
        this.checkpoints = checkpoints;
    }

    public Integer getNbplace() {
        return nbplace;
    }

    public void setNbplace(Integer nbplace) {
        this.nbplace = nbplace;
    }

    @XmlTransient
    public Collection<Ville> getVilleCollection() {
        return villeCollection;
    }

    public void setVilleCollection(Collection<Ville> villeCollection) {
        this.villeCollection = villeCollection;
    }

    public Ville getVilIdVille() {
        return vilIdVille;
    }

    public void setVilIdVille(Ville vilIdVille) {
        this.vilIdVille = vilIdVille;
    }

    public Ville getIdVilleArr() {
        return idVilleArr;
    }

    public void setIdVilleArr(Ville idVilleArr) {
        this.idVilleArr = idVilleArr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPub != null ? idPub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Covoiturage)) {
            return false;
        }
        Covoiturage other = (Covoiturage) object;
        if ((this.idPub == null && other.idPub != null) || (this.idPub != null && !this.idPub.equals(other.idPub))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Covoiturage[ idPub=" + idPub + " ]";
    }
    
}
