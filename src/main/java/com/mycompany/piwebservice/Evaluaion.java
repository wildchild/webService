/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "evaluaion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evaluaion.findAll", query = "SELECT e FROM Evaluaion e")
    , @NamedQuery(name = "Evaluaion.findByIdEv", query = "SELECT e FROM Evaluaion e WHERE e.idEv = :idEv")
    , @NamedQuery(name = "Evaluaion.findByIdUsr", query = "SELECT e FROM Evaluaion e WHERE e.idUsr = :idUsr")
    , @NamedQuery(name = "Evaluaion.findByIdPub", query = "SELECT e FROM Evaluaion e WHERE e.idPub = :idPub")
    , @NamedQuery(name = "Evaluaion.findByNote", query = "SELECT e FROM Evaluaion e WHERE e.note = :note")})
public class Evaluaion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_EV")
    private Integer idEv;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USR")
    private int idUsr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PUB")
    private int idPub;
    @Basic(optional = false)
    @NotNull
    @Column(name = "note")
    private int note;

    public Evaluaion() {
    }

    public Evaluaion(Integer idEv) {
        this.idEv = idEv;
    }

    public Evaluaion(Integer idEv, int idUsr, int idPub, int note) {
        this.idEv = idEv;
        this.idUsr = idUsr;
        this.idPub = idPub;
        this.note = note;
    }

    public Integer getIdEv() {
        return idEv;
    }

    public void setIdEv(Integer idEv) {
        this.idEv = idEv;
    }

    public int getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(int idUsr) {
        this.idUsr = idUsr;
    }

    public int getIdPub() {
        return idPub;
    }

    public void setIdPub(int idPub) {
        this.idPub = idPub;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEv != null ? idEv.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluaion)) {
            return false;
        }
        Evaluaion other = (Evaluaion) object;
        if ((this.idEv == null && other.idEv != null) || (this.idEv != null && !this.idEv.equals(other.idEv))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Evaluaion[ idEv=" + idEv + " ]";
    }
    
}
