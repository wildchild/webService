/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "evenement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evenement.findAll", query = "SELECT e FROM Evenement e")
    , @NamedQuery(name = "Evenement.findByIdPub", query = "SELECT e FROM Evenement e WHERE e.idPub = :idPub")
    , @NamedQuery(name = "Evenement.findByIdUsr", query = "SELECT e FROM Evenement e WHERE e.idUsr = :idUsr")
    , @NamedQuery(name = "Evenement.findByDatepub", query = "SELECT e FROM Evenement e WHERE e.datepub = :datepub")
    , @NamedQuery(name = "Evenement.findByEtat", query = "SELECT e FROM Evenement e WHERE e.etat = :etat")
    , @NamedQuery(name = "Evenement.findByNomEvent", query = "SELECT e FROM Evenement e WHERE e.nomEvent = :nomEvent")
    , @NamedQuery(name = "Evenement.findByDatedebut", query = "SELECT e FROM Evenement e WHERE e.datedebut = :datedebut")
    , @NamedQuery(name = "Evenement.findByDatefin", query = "SELECT e FROM Evenement e WHERE e.datefin = :datefin")
    , @NamedQuery(name = "Evenement.findByLieu", query = "SELECT e FROM Evenement e WHERE e.lieu = :lieu")
    , @NamedQuery(name = "Evenement.findByPlaceDispo", query = "SELECT e FROM Evenement e WHERE e.placeDispo = :placeDispo")})
public class Evenement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PUB")
    private Integer idPub;
    @Column(name = "ID_USR")
    private Integer idUsr;
    @Column(name = "DATEPUB")
    @Temporal(TemporalType.DATE)
    private Date datepub;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 20)
    @Column(name = "ETAT")
    private String etat;
    @Size(max = 50)
    @Column(name = "NOM_EVENT")
    private String nomEvent;
    @Column(name = "DATEDEBUT")
    @Temporal(TemporalType.DATE)
    private Date datedebut;
    @Column(name = "DATEFIN")
    @Temporal(TemporalType.DATE)
    private Date datefin;
    @Size(max = 50)
    @Column(name = "LIEU")
    private String lieu;
    @Lob
    @Size(max = 65535)
    @Column(name = "AFFICHE")
    private String affiche;
    @Basic(optional = false)
    @NotNull
    @Column(name = "place_dispo")
    private int placeDispo;
    @ManyToMany(mappedBy = "evenementCollection")
    private Collection<Ville> villeCollection;

    public Evenement() {
    }

    public Evenement(Integer idPub) {
        this.idPub = idPub;
    }

    public Evenement(Integer idPub, int placeDispo) {
        this.idPub = idPub;
        this.placeDispo = placeDispo;
    }

    public Integer getIdPub() {
        return idPub;
    }

    public void setIdPub(Integer idPub) {
        this.idPub = idPub;
    }

    public Integer getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(Integer idUsr) {
        this.idUsr = idUsr;
    }

    public Date getDatepub() {
        return datepub;
    }

    public void setDatepub(Date datepub) {
        this.datepub = datepub;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getNomEvent() {
        return nomEvent;
    }

    public void setNomEvent(String nomEvent) {
        this.nomEvent = nomEvent;
    }

    public Date getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(Date datedebut) {
        this.datedebut = datedebut;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getAffiche() {
        return affiche;
    }

    public void setAffiche(String affiche) {
        this.affiche = affiche;
    }

    public int getPlaceDispo() {
        return placeDispo;
    }

    public void setPlaceDispo(int placeDispo) {
        this.placeDispo = placeDispo;
    }

    @XmlTransient
    public Collection<Ville> getVilleCollection() {
        return villeCollection;
    }

    public void setVilleCollection(Collection<Ville> villeCollection) {
        this.villeCollection = villeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPub != null ? idPub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evenement)) {
            return false;
        }
        Evenement other = (Evenement) object;
        if ((this.idPub == null && other.idPub != null) || (this.idPub != null && !this.idPub.equals(other.idPub))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Evenement[ idPub=" + idPub + " ]";
    }
    
}
