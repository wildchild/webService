/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "gouvernerat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gouvernerat.findAll", query = "SELECT g FROM Gouvernerat g")
    , @NamedQuery(name = "Gouvernerat.findByIdGov", query = "SELECT g FROM Gouvernerat g WHERE g.idGov = :idGov")
    , @NamedQuery(name = "Gouvernerat.findByLibGov", query = "SELECT g FROM Gouvernerat g WHERE g.libGov = :libGov")})
public class Gouvernerat implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_GOV")
    private Integer idGov;
    @Size(max = 100)
    @Column(name = "LIB_GOV")
    private String libGov;
    @OneToMany(mappedBy = "idGov")
    private Collection<Ville> villeCollection;

    public Gouvernerat() {
    }

    public Gouvernerat(Integer idGov) {
        this.idGov = idGov;
    }

    public Integer getIdGov() {
        return idGov;
    }

    public void setIdGov(Integer idGov) {
        this.idGov = idGov;
    }

    public String getLibGov() {
        return libGov;
    }

    public void setLibGov(String libGov) {
        this.libGov = libGov;
    }

    @XmlTransient
    public Collection<Ville> getVilleCollection() {
        return villeCollection;
    }

    public void setVilleCollection(Collection<Ville> villeCollection) {
        this.villeCollection = villeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGov != null ? idGov.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gouvernerat)) {
            return false;
        }
        Gouvernerat other = (Gouvernerat) object;
        if ((this.idGov == null && other.idGov != null) || (this.idGov != null && !this.idGov.equals(other.idGov))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Gouvernerat[ idGov=" + idGov + " ]";
    }
    
}
