/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "reclamation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reclamation.findAll", query = "SELECT r FROM Reclamation r")
    , @NamedQuery(name = "Reclamation.findByIdPub", query = "SELECT r FROM Reclamation r WHERE r.idPub = :idPub")
    , @NamedQuery(name = "Reclamation.findByDatepub", query = "SELECT r FROM Reclamation r WHERE r.datepub = :datepub")
    , @NamedQuery(name = "Reclamation.findBySujetRec", query = "SELECT r FROM Reclamation r WHERE r.sujetRec = :sujetRec")
    , @NamedQuery(name = "Reclamation.findByEtat", query = "SELECT r FROM Reclamation r WHERE r.etat = :etat")
    , @NamedQuery(name = "Reclamation.findByTypeRec", query = "SELECT r FROM Reclamation r WHERE r.typeRec = :typeRec")})
public class Reclamation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PUB")
    private Integer idPub;
    @Column(name = "DATEPUB")
    @Temporal(TemporalType.DATE)
    private Date datepub;
    @Size(max = 100)
    @Column(name = "SUJET_REC")
    private String sujetRec;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 20)
    @Column(name = "ETAT")
    private String etat;
    @Size(max = 50)
    @Column(name = "TYPE_REC")
    private String typeRec;
    @JoinColumn(name = "USE_ID_USR", referencedColumnName = "id")
    @ManyToOne
    private User useIdUsr;
    @JoinColumn(name = "ID_USR", referencedColumnName = "id")
    @ManyToOne
    private User idUsr;

    public Reclamation() {
    }

    public Reclamation(Integer idPub) {
        this.idPub = idPub;
    }

    public Integer getIdPub() {
        return idPub;
    }

    public void setIdPub(Integer idPub) {
        this.idPub = idPub;
    }

    public Date getDatepub() {
        return datepub;
    }

    public void setDatepub(Date datepub) {
        this.datepub = datepub;
    }

    public String getSujetRec() {
        return sujetRec;
    }

    public void setSujetRec(String sujetRec) {
        this.sujetRec = sujetRec;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getTypeRec() {
        return typeRec;
    }

    public void setTypeRec(String typeRec) {
        this.typeRec = typeRec;
    }

    public User getUseIdUsr() {
        return useIdUsr;
    }

    public void setUseIdUsr(User useIdUsr) {
        this.useIdUsr = useIdUsr;
    }

    public User getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(User idUsr) {
        this.idUsr = idUsr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPub != null ? idPub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reclamation)) {
            return false;
        }
        Reclamation other = (Reclamation) object;
        if ((this.idPub == null && other.idPub != null) || (this.idPub != null && !this.idPub.equals(other.idPub))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Reclamation[ idPub=" + idPub + " ]";
    }
    
}
