/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cobwi
 */
@Entity
@Table(name = "ville")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ville.findAll", query = "SELECT v FROM Ville v")
    , @NamedQuery(name = "Ville.findByIdVille", query = "SELECT v FROM Ville v WHERE v.idVille = :idVille")
    , @NamedQuery(name = "Ville.findByNomVille", query = "SELECT v FROM Ville v WHERE v.nomVille = :nomVille")})
public class Ville implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_VILLE")
    private Integer idVille;
    @Size(max = 250)
    @Column(name = "NOM_VILLE")
    private String nomVille;
    @JoinTable(name = "passer", joinColumns = {
        @JoinColumn(name = "ID_VILLE", referencedColumnName = "ID_VILLE")}, inverseJoinColumns = {
        @JoinColumn(name = "ID_PUB", referencedColumnName = "ID_PUB")})
    @ManyToMany
    private Collection<Covoiturage> covoiturageCollection;
    @JoinTable(name = "situer_event", joinColumns = {
        @JoinColumn(name = "ID_VILLE_S", referencedColumnName = "ID_VILLE")}, inverseJoinColumns = {
        @JoinColumn(name = "ID_PUB_S", referencedColumnName = "ID_PUB")})
    @ManyToMany
    private Collection<Evenement> evenementCollection;
    @JoinColumn(name = "ID_GOV", referencedColumnName = "ID_GOV")
    @ManyToOne
    private Gouvernerat idGov;
    @OneToMany(mappedBy = "idVille")
    private Collection<Colocation> colocationCollection;
    @OneToMany(mappedBy = "vilIdVille")
    private Collection<Covoiturage> covoiturageCollection1;
    @OneToMany(mappedBy = "idVilleArr")
    private Collection<Covoiturage> covoiturageCollection2;

    public Ville() {
    }

    public Ville(Integer idVille) {
        this.idVille = idVille;
    }

    public Integer getIdVille() {
        return idVille;
    }

    public void setIdVille(Integer idVille) {
        this.idVille = idVille;
    }

    public String getNomVille() {
        return nomVille;
    }

    public void setNomVille(String nomVille) {
        this.nomVille = nomVille;
    }

    @XmlTransient
    public Collection<Covoiturage> getCovoiturageCollection() {
        return covoiturageCollection;
    }

    public void setCovoiturageCollection(Collection<Covoiturage> covoiturageCollection) {
        this.covoiturageCollection = covoiturageCollection;
    }

    @XmlTransient
    public Collection<Evenement> getEvenementCollection() {
        return evenementCollection;
    }

    public void setEvenementCollection(Collection<Evenement> evenementCollection) {
        this.evenementCollection = evenementCollection;
    }

    public Gouvernerat getIdGov() {
        return idGov;
    }

    public void setIdGov(Gouvernerat idGov) {
        this.idGov = idGov;
    }

    @XmlTransient
    public Collection<Colocation> getColocationCollection() {
        return colocationCollection;
    }

    public void setColocationCollection(Collection<Colocation> colocationCollection) {
        this.colocationCollection = colocationCollection;
    }

    @XmlTransient
    public Collection<Covoiturage> getCovoiturageCollection1() {
        return covoiturageCollection1;
    }

    public void setCovoiturageCollection1(Collection<Covoiturage> covoiturageCollection1) {
        this.covoiturageCollection1 = covoiturageCollection1;
    }

    @XmlTransient
    public Collection<Covoiturage> getCovoiturageCollection2() {
        return covoiturageCollection2;
    }

    public void setCovoiturageCollection2(Collection<Covoiturage> covoiturageCollection2) {
        this.covoiturageCollection2 = covoiturageCollection2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVille != null ? idVille.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ville)) {
            return false;
        }
        Ville other = (Ville) object;
        if ((this.idVille == null && other.idVille != null) || (this.idVille != null && !this.idVille.equals(other.idVille))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.piwebservice.Ville[ idVille=" + idVille + " ]";
    }
    
}
