/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.piwebservice.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author cobwi
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.mycompany.piwebservice.service.AideFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.ColocationFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.CommentaireFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.CovoiturageFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.EvaluaionFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.EvenementFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.FosUserFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.GouverneratFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.QuestionFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.ReclamationFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.ReservationFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.UserFacadeREST.class);
        resources.add(com.mycompany.piwebservice.service.VilleFacadeREST.class);
    }
    
}
