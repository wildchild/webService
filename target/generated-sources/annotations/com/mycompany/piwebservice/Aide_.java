package com.mycompany.piwebservice;

import com.mycompany.piwebservice.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:38")
@StaticMetamodel(Aide.class)
public class Aide_ { 

    public static volatile SingularAttribute<Aide, Integer> idPub;
    public static volatile SingularAttribute<Aide, String> document;
    public static volatile SingularAttribute<Aide, Date> datepub;
    public static volatile SingularAttribute<Aide, String> description;
    public static volatile SingularAttribute<Aide, String> section;
    public static volatile SingularAttribute<Aide, String> matiere;
    public static volatile SingularAttribute<Aide, User> idUsr;

}