package com.mycompany.piwebservice;

import com.mycompany.piwebservice.Ville;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:37")
@StaticMetamodel(Colocation.class)
public class Colocation_ { 

    public static volatile SingularAttribute<Colocation, Double> loyermensuel;
    public static volatile SingularAttribute<Colocation, String> typechambre;
    public static volatile SingularAttribute<Colocation, String> imgcouverture;
    public static volatile SingularAttribute<Colocation, Date> datepub;
    public static volatile SingularAttribute<Colocation, String> description;
    public static volatile SingularAttribute<Colocation, String> typelogement;
    public static volatile SingularAttribute<Colocation, Date> datedisponibilite;
    public static volatile SingularAttribute<Colocation, String> etat;
    public static volatile SingularAttribute<Colocation, Integer> idUsr;
    public static volatile SingularAttribute<Colocation, Integer> idPub;
    public static volatile SingularAttribute<Colocation, Integer> nbchambre;
    public static volatile SingularAttribute<Colocation, String> commodite;
    public static volatile SingularAttribute<Colocation, Ville> idVille;
    public static volatile SingularAttribute<Colocation, String> lieu;

}