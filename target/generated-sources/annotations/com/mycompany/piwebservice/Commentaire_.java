package com.mycompany.piwebservice;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:38")
@StaticMetamodel(Commentaire.class)
public class Commentaire_ { 

    public static volatile SingularAttribute<Commentaire, Integer> idCom;
    public static volatile SingularAttribute<Commentaire, String> contenuCom;
    public static volatile SingularAttribute<Commentaire, Integer> idPub;
    public static volatile SingularAttribute<Commentaire, Date> dateCom;
    public static volatile SingularAttribute<Commentaire, String> photoprofile;
    public static volatile SingularAttribute<Commentaire, Integer> idUsr;
    public static volatile SingularAttribute<Commentaire, String> username;

}