package com.mycompany.piwebservice;

import com.mycompany.piwebservice.Ville;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:37")
@StaticMetamodel(Covoiturage.class)
public class Covoiturage_ { 

    public static volatile SingularAttribute<Covoiturage, String> lieudepart;
    public static volatile SingularAttribute<Covoiturage, Double> prix;
    public static volatile CollectionAttribute<Covoiturage, Ville> villeCollection;
    public static volatile SingularAttribute<Covoiturage, Date> datedepart;
    public static volatile SingularAttribute<Covoiturage, Date> datepub;
    public static volatile SingularAttribute<Covoiturage, String> description;
    public static volatile SingularAttribute<Covoiturage, String> checkpoints;
    public static volatile SingularAttribute<Covoiturage, String> etat;
    public static volatile SingularAttribute<Covoiturage, Integer> idUsr;
    public static volatile SingularAttribute<Covoiturage, Ville> idVilleArr;
    public static volatile SingularAttribute<Covoiturage, Integer> idPub;
    public static volatile SingularAttribute<Covoiturage, Integer> nbplace;
    public static volatile SingularAttribute<Covoiturage, String> lieuarrive;
    public static volatile SingularAttribute<Covoiturage, Ville> vilIdVille;

}