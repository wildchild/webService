package com.mycompany.piwebservice;

import com.mycompany.piwebservice.Ville;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:38")
@StaticMetamodel(Evenement.class)
public class Evenement_ { 

    public static volatile SingularAttribute<Evenement, String> affiche;
    public static volatile SingularAttribute<Evenement, Integer> placeDispo;
    public static volatile SingularAttribute<Evenement, Integer> idPub;
    public static volatile CollectionAttribute<Evenement, Ville> villeCollection;
    public static volatile SingularAttribute<Evenement, String> nomEvent;
    public static volatile SingularAttribute<Evenement, Date> datepub;
    public static volatile SingularAttribute<Evenement, Date> datedebut;
    public static volatile SingularAttribute<Evenement, String> description;
    public static volatile SingularAttribute<Evenement, Date> datefin;
    public static volatile SingularAttribute<Evenement, String> etat;
    public static volatile SingularAttribute<Evenement, Integer> idUsr;
    public static volatile SingularAttribute<Evenement, String> lieu;

}