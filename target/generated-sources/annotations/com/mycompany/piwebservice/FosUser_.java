package com.mycompany.piwebservice;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:37")
@StaticMetamodel(FosUser.class)
public class FosUser_ { 

    public static volatile SingularAttribute<FosUser, Date> lastLogin;
    public static volatile SingularAttribute<FosUser, String> salt;
    public static volatile SingularAttribute<FosUser, String> roles;
    public static volatile SingularAttribute<FosUser, String> telephone;
    public static volatile SingularAttribute<FosUser, String> emailCanonical;
    public static volatile SingularAttribute<FosUser, Date> passwordRequestedAt;
    public static volatile SingularAttribute<FosUser, String> nom;
    public static volatile SingularAttribute<FosUser, Boolean> enabled;
    public static volatile SingularAttribute<FosUser, String> password;
    public static volatile SingularAttribute<FosUser, Date> datenaissance;
    public static volatile SingularAttribute<FosUser, String> confirmationToken;
    public static volatile SingularAttribute<FosUser, Integer> id;
    public static volatile SingularAttribute<FosUser, String> usernameCanonical;
    public static volatile SingularAttribute<FosUser, String> photoprofile;
    public static volatile SingularAttribute<FosUser, String> prenom;
    public static volatile SingularAttribute<FosUser, String> email;
    public static volatile SingularAttribute<FosUser, String> username;

}