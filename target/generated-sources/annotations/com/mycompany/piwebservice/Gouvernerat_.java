package com.mycompany.piwebservice;

import com.mycompany.piwebservice.Ville;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:37")
@StaticMetamodel(Gouvernerat.class)
public class Gouvernerat_ { 

    public static volatile SingularAttribute<Gouvernerat, Integer> idGov;
    public static volatile CollectionAttribute<Gouvernerat, Ville> villeCollection;
    public static volatile SingularAttribute<Gouvernerat, String> libGov;

}