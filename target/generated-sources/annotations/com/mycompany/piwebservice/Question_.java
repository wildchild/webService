package com.mycompany.piwebservice;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:38")
@StaticMetamodel(Question.class)
public class Question_ { 

    public static volatile SingularAttribute<Question, Integer> idPub;
    public static volatile SingularAttribute<Question, Date> datepub;
    public static volatile SingularAttribute<Question, String> description;
    public static volatile SingularAttribute<Question, String> sujet;
    public static volatile SingularAttribute<Question, String> etat;
    public static volatile SingularAttribute<Question, Integer> idUsr;

}