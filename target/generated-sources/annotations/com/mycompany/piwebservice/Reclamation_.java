package com.mycompany.piwebservice;

import com.mycompany.piwebservice.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:37")
@StaticMetamodel(Reclamation.class)
public class Reclamation_ { 

    public static volatile SingularAttribute<Reclamation, String> typeRec;
    public static volatile SingularAttribute<Reclamation, User> useIdUsr;
    public static volatile SingularAttribute<Reclamation, Integer> idPub;
    public static volatile SingularAttribute<Reclamation, Date> datepub;
    public static volatile SingularAttribute<Reclamation, String> description;
    public static volatile SingularAttribute<Reclamation, String> etat;
    public static volatile SingularAttribute<Reclamation, String> sujetRec;
    public static volatile SingularAttribute<Reclamation, User> idUsr;

}