package com.mycompany.piwebservice;

import com.mycompany.piwebservice.Aide;
import com.mycompany.piwebservice.Reclamation;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:38")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, Date> lastLogin;
    public static volatile SingularAttribute<User, String> salt;
    public static volatile SingularAttribute<User, String> roles;
    public static volatile CollectionAttribute<User, Reclamation> reclamationCollection1;
    public static volatile SingularAttribute<User, String> telephone;
    public static volatile CollectionAttribute<User, Aide> aideCollection;
    public static volatile SingularAttribute<User, String> emailCanonical;
    public static volatile SingularAttribute<User, Date> passwordRequestedAt;
    public static volatile SingularAttribute<User, String> nom;
    public static volatile SingularAttribute<User, Boolean> enabled;
    public static volatile SingularAttribute<User, String> profilePicture;
    public static volatile SingularAttribute<User, Date> createdAt;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, Date> datenaissance;
    public static volatile SingularAttribute<User, String> confirmationToken;
    public static volatile CollectionAttribute<User, Reclamation> reclamationCollection;
    public static volatile SingularAttribute<User, Integer> id;
    public static volatile SingularAttribute<User, String> usernameCanonical;
    public static volatile SingularAttribute<User, String> photoprofile;
    public static volatile SingularAttribute<User, String> prenom;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, String> username;
    public static volatile SingularAttribute<User, Date> updatedAt;

}