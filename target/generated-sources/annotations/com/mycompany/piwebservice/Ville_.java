package com.mycompany.piwebservice;

import com.mycompany.piwebservice.Colocation;
import com.mycompany.piwebservice.Covoiturage;
import com.mycompany.piwebservice.Evenement;
import com.mycompany.piwebservice.Gouvernerat;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-25T13:19:38")
@StaticMetamodel(Ville.class)
public class Ville_ { 

    public static volatile CollectionAttribute<Ville, Evenement> evenementCollection;
    public static volatile CollectionAttribute<Ville, Covoiturage> covoiturageCollection1;
    public static volatile CollectionAttribute<Ville, Covoiturage> covoiturageCollection2;
    public static volatile SingularAttribute<Ville, String> nomVille;
    public static volatile SingularAttribute<Ville, Gouvernerat> idGov;
    public static volatile SingularAttribute<Ville, Integer> idVille;
    public static volatile CollectionAttribute<Ville, Colocation> colocationCollection;
    public static volatile CollectionAttribute<Ville, Covoiturage> covoiturageCollection;

}